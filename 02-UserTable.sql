﻿CREATE TABLE `BallastLane`.`Project` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `ExternalId` VARCHAR(45) NOT NULL,
  `Name` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(1000) NULL,
  `Owner` VARCHAR(50) NULL,
  `UserId` INT NULL,  
  PRIMARY KEY (`Id`));