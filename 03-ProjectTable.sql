﻿CREATE TABLE `BallastLane`.`User` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(50) NULL,
  `LastName` VARCHAR(50) NULL,
  `Email` VARCHAR(50) NULL,
  `Password` VARCHAR(15) NULL,
  `ExternalId` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`));
