﻿using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Application.Authentication.Common;
using BallastLane.Application.Authentication.Queries.Login;
using BallastLane.Application.Project.Common;
using BallastLane.Contracts.Authentication;
using BallastLane.Contracts.Project;
using Mapster;

namespace BallastLane.Api.Common.Mapping;

public class AuthenticationMappingProfile : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<RegisterUserRequest, RegisterCommand>();

        config.NewConfig<LoginRequest, LoginQuery>();

        config.NewConfig<AuthenticationResult, AuthResponse>()
            .Map(dest => dest, src => src.User);

        config.NewConfig<ProjectResult, ProjectResponse>()
            .Map(dest => dest, src => src.Project);
    }
}
