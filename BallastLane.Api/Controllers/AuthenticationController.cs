﻿using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Application.Authentication.Queries.Login;
using BallastLane.Contracts.Authentication;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BallastLane.Api.Controllers
{
    [ApiController]
    [Route("/api/auth/")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ISender _mediator;
        private readonly IMapper _mapper;
        
        public AuthenticationController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserRequest request) 
        {
            var command = _mapper.Map<RegisterCommand>(request);
            var authResult = await _mediator.Send(command);

            var response = _mapper.Map<AuthResponse>(authResult);
            return Ok(response);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var query = _mapper.Map<LoginQuery>(request);
            var authResult = await _mediator.Send(query);

            var response = _mapper.Map<AuthResponse>(authResult); 
            return Ok(response);
        }
    }
}
