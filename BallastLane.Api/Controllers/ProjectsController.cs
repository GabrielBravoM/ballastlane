﻿using BallastLane.Application.Project.Commands;
using BallastLane.Contracts.Project;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BallastLane.Api.Controllers
{
    [ApiController]
    [Route("/api/projects/")]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private readonly ISender _mediator;
        private readonly IMapper _mapper;

        public ProjectsController(ISender mediator, IMapper mapper) {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {            
            var projectResult = await _mediator.Send(new GetProjectsCommand());

            var response = _mapper.Map<IEnumerable<ProjectResponse>>(projectResult);
            return Ok(response);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateProjectRequest request) 
        {
            var command = _mapper.Map<CreateProjectCommand>(request);
            var projectResult = await _mediator.Send(command);

            var response = _mapper.Map<ProjectResponse>(projectResult);
            return Ok(response);
        }

        [HttpDelete("{externalId}")]
        public async Task<IActionResult> DeleteAsync(string externalId)
        {
            var projectResult = await _mediator.Send(new DeleteProjectCommand(externalId));

            var response = _mapper.Map<ProjectResponse>(projectResult);
            return Ok(response);
        }

        [HttpPut("{externalId}")]
        public async Task<IActionResult> UpdateAsync(string externalId, [FromBody] UpdateProjectRequest request)
        {
            var command = _mapper.Map<UpdateProjectCommand>(request);
            var projectResult = await _mediator.Send(command);

            var response = _mapper.Map<ProjectResponse>(projectResult);
            return Ok(response);
        }

    }
}
