﻿using BallastLane.Api.Common.Mapping;
using BallastLane.Api.Validators;
using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Contracts.Authentication;
using FluentValidation;
using FluentValidation.AspNetCore;
using System.Reflection;

namespace BallastLane.Api;

public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services.AddControllers()
            .AddFluentValidation(fv =>
            {
                fv.ImplicitlyValidateChildProperties = true;
                fv.RegisterValidatorsFromAssemblyContaining<RegisterCommandValidator>();
            });
        services.AddMappings();
        return services;
    }
}
