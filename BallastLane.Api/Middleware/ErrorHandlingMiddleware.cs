﻿using Newtonsoft.Json;
using System.Net;

namespace BallastLane.Api.Middleware;

public class ErrorHandlingMiddleware
{
    private readonly RequestDelegate _nText;

    public ErrorHandlingMiddleware(RequestDelegate nText)
    {
        _nText = nText;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _nText(context);
        }
        catch (Exception ex) {
            await HandleExceptionAsync(context, ex);
        }
    }

    private static Task HandleExceptionAsync(HttpContext context, Exception ex)
    {
        var code = HttpStatusCode.InternalServerError;
        var result = JsonConvert.SerializeObject(new { error = "Error processing your request" });
        context.Response.ContentType= "application/json";
        context.Response.StatusCode = (int)code;
        return context.Response.WriteAsync(result);
    }
}
