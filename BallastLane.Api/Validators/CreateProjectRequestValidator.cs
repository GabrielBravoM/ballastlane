﻿using BallastLane.Contracts.Project;
using FluentValidation;

namespace BallastLane.Api.Validators
{
    public class CreateProjectRequestValidator : AbstractValidator<CreateProjectRequest>
    {
        public CreateProjectRequestValidator()
        {
            RuleFor(x => x.UserExternalId).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Owner).NotEmpty();
        }
    }
}
