﻿using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Contracts.Authentication;
using FluentValidation;

namespace BallastLane.Api.Validators
{
    public class RegisterCommandValidator : AbstractValidator<RegisterUserRequest>
    {
        public RegisterCommandValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
