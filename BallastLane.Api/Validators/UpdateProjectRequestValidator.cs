﻿using BallastLane.Contracts.Project;
using FluentValidation;

namespace BallastLane.Api.Validators
{
    public class UpdateProjectRequestValidator : AbstractValidator<UpdateProjectRequest>
    {
        public UpdateProjectRequestValidator()
        {
            RuleFor(x => x.ExternalId).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Owner).NotEmpty();
        }
    }
}
