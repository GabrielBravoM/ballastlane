﻿using BallastLane.Application.Authentication.Common;
using BallastLane.Application.Common.Errors;
using BallastLane.Application.Common.Interfaces.Authentication;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Domain.Entities;
using MediatR;

namespace BallastLane.Application.Authentication.Commands.Register
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, AuthenticationResult>
    {
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IUserRepository _userRepository;

        public RegisterCommandHandler(ITokenGenerator tokenGenerator, IUserRepository userRepository)
        {
            _tokenGenerator = tokenGenerator;
            _userRepository = userRepository;
        }

        public async Task<AuthenticationResult> Handle(RegisterCommand command, CancellationToken cancellationToken)
        {
            if (_userRepository.GetUserByEmail(command.Email) is not null)
            {
                throw new DuplicatedEmailException();
            }

            var externalId = Guid.NewGuid().ToString();

            var user = new UserEntity
            {
                ExternalId = externalId,
                FirstName = command.FirstName,
                LastName = command.LastName,
                Email = command.Email,
                Password = command.Password
            };

            _userRepository.CreateUser(user);

            var token = _tokenGenerator.GenerateToken(user);

            return new AuthenticationResult
            (
                user,
                token
            );
        }
    }
}
