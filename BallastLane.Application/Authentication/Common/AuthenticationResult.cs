﻿using BallastLane.Domain.Entities;

namespace BallastLane.Application.Authentication.Common
{
    public record AuthenticationResult(UserEntity User, string Token);
}
