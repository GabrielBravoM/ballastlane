﻿using BallastLane.Application.Authentication.Common;
using MediatR;

namespace BallastLane.Application.Authentication.Queries.Login
{
    public record LoginQuery(
        string Email,
        string Password) : IRequest<AuthenticationResult>;
}
