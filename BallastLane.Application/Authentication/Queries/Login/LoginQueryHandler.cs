﻿using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Application.Authentication.Common;
using BallastLane.Application.Common.Errors;
using BallastLane.Application.Common.Interfaces.Authentication;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Domain.Entities;
using MediatR;

namespace BallastLane.Application.Authentication.Queries.Login
{
    public class LoginQueryHandler : IRequestHandler<LoginQuery, AuthenticationResult>
    {
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IUserRepository _userRepository;

        public LoginQueryHandler(ITokenGenerator tokenGenerator, IUserRepository userRepository)
        {
            _tokenGenerator = tokenGenerator;
            _userRepository = userRepository;
        }

        public async Task<AuthenticationResult> Handle(LoginQuery query, CancellationToken cancellationToken)
        {
            if (_userRepository.GetUserByEmail(query.Email) is not UserEntity user)
            {
                throw new IncorrectLoginException();
            }

            if (user.Password != query.Password)
            {
                throw new IncorrectLoginException();
            }

            var token = _tokenGenerator.GenerateToken(user);

            return new AuthenticationResult
            (
                user,
                token
            );
        }
    }
}
