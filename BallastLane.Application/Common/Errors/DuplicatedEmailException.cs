﻿using System.Net;

namespace BallastLane.Application.Common.Errors;

public class DuplicatedEmailException : Exception, IServiceException
{
    public HttpStatusCode StatusCode => HttpStatusCode.Conflict;

    public string ErrorMessage => "Email already exists";
}
