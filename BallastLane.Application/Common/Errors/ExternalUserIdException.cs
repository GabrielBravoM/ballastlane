﻿using System.Net;

namespace BallastLane.Application.Common.Errors
{
    public class ExternalUserIdException : Exception, IServiceException
    {
        public HttpStatusCode StatusCode => HttpStatusCode.NotFound;

        public string ErrorMessage => "User External id does not exist";
    }
}
