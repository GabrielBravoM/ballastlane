﻿using System.Net;

namespace BallastLane.Application.Common.Errors
{
    public class IncorrectLoginException : Exception, IServiceException
    {
        public HttpStatusCode StatusCode => HttpStatusCode.Conflict;

        public string ErrorMessage => "Email or password are incorrect";
    }
}
