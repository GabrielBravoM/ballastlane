﻿using BallastLane.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BallastLane.Application.Common.Interfaces.Authentication
{
    public interface ITokenGenerator
    {
        string GenerateToken(UserEntity user);
    }
}
