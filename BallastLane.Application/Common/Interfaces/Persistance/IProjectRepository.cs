﻿using BallastLane.Domain.Entities;

namespace BallastLane.Application.Common.Interfaces.Persistance
{
    public interface IProjectRepository
    {
        IEnumerable<ProjectEntity>? GetAll();
        void CreateProject(ProjectEntity project);
        void UpdateProject(ProjectEntity project);
        void DeleteProject(string externalId);
        ProjectEntity? GetByExternalId(string externalId);
    }
}
