﻿using BallastLane.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BallastLane.Application.Common.Interfaces.Persistance
{
    public interface IUserRepository
    {
        UserEntity? GetUserByEmail(string email);
        UserEntity? GetUserByExternalId(string externalId);
        void CreateUser(UserEntity user);
    }
}
