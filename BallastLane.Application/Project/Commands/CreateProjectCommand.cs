﻿using BallastLane.Application.Project.Common;
using MediatR;

namespace BallastLane.Application.Project.Commands
{
    public record CreateProjectCommand(
        string userExternalId,
        string Name,
        string Description,
        string Owner) : IRequest<ProjectResult>;

}
