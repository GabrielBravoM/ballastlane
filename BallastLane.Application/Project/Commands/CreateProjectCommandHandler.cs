﻿using BallastLane.Application.Common.Errors;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Application.Project.Common;
using BallastLane.Domain.Entities;
using MediatR;

namespace BallastLane.Application.Project.Commands
{    
    public class CreateProjectCommandHandler : IRequestHandler<CreateProjectCommand, ProjectResult>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;

        public CreateProjectCommandHandler(IProjectRepository projectRepository, IUserRepository userRepository)
        {
            _projectRepository = projectRepository;
            _userRepository = userRepository;
        }

        public async Task<ProjectResult> Handle(CreateProjectCommand command, CancellationToken cancellationToken)
        {
            var user = _userRepository.GetUserByExternalId(command.userExternalId);
            if (user is null)
            {
                throw new ExternalUserIdException();
            }

            var projectExternalId = Guid.NewGuid().ToString();

            var project = new ProjectEntity
            {
                ExternalId = projectExternalId,
                UserId = user.Id,
                Name = command.Name,
                Description = command.Description,
                Owner= command.Owner,
            };

            _projectRepository.CreateProject(project);
            
            return new ProjectResult
            (
                Project: project
                //UserExternalId: command.userExternalId
            );
        }
    }
}
