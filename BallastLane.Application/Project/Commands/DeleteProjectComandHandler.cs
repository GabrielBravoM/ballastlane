﻿using BallastLane.Application.Common.Errors;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Application.Project.Common;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BallastLane.Application.Project.Commands
{
    public class DeleteProjectComandHandler : IRequestHandler<DeleteProjectCommand, ProjectResult>
    {
        private readonly IProjectRepository _projectRepository;

        public DeleteProjectComandHandler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<ProjectResult> Handle(DeleteProjectCommand command, CancellationToken cancellationToken)
        {
            var project = _projectRepository.GetByExternalId(command.ExternalId);
            if (project is null)
            {
                throw new ProjectNoExistException();
            }

            _projectRepository.DeleteProject(command.ExternalId);

            return new ProjectResult
            (
                Project: project
            );

        }
    }
}
