﻿using BallastLane.Application.Project.Common;
using MediatR;

namespace BallastLane.Application.Project.Commands
{
    public record GetProjectsCommand() : IRequest<IEnumerable<ProjectResult>>;
}
