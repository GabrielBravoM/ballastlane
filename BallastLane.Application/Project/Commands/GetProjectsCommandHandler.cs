﻿using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Application.Project.Common;
using MediatR;

namespace BallastLane.Application.Project.Commands
{
    public class GetProjectsCommandHandler : IRequestHandler<GetProjectsCommand, IEnumerable<ProjectResult>>
    {
        private readonly IProjectRepository _projectRepository;

        public GetProjectsCommandHandler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task<IEnumerable<ProjectResult>> Handle(GetProjectsCommand request, CancellationToken cancellationToken)
        {
            var projects = _projectRepository.GetAll();

            var projectsResult = new List<ProjectResult>();

            projects?.ToList().ForEach(project =>
            {
                projectsResult.Add(new ProjectResult(project)); //TODO: , Guid.NewGuid()
            });

            return projectsResult;
        }
    }
}
