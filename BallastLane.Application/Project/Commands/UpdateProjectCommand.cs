﻿using BallastLane.Application.Project.Common;
using MediatR;

namespace BallastLane.Application.Project.Commands
{
    public record UpdateProjectCommand(
        string ExternalId,
        string Name,
        string Description,
        string Owner) : IRequest<ProjectResult>;
}
