﻿using BallastLane.Application.Common.Errors;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Application.Project.Common;
using MediatR;

namespace BallastLane.Application.Project.Commands
{
    public class UpdateProjectCommandHandler : IRequestHandler<UpdateProjectCommand, ProjectResult>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;

        public UpdateProjectCommandHandler(IProjectRepository projectRepository, IUserRepository userRepository)
        {
            _projectRepository = projectRepository;
            _userRepository = userRepository;
        }

        public async Task<ProjectResult> Handle(UpdateProjectCommand command, CancellationToken cancellationToken)
        {
            var project = _projectRepository.GetByExternalId(command.ExternalId);
            if (project is null)
            {
                throw new ProjectNoExistException();
            }

            project.Name = command.Name;
            project.Description = command.Description;
            project.Owner= command.Owner;

            _projectRepository.UpdateProject(project);

            return new ProjectResult
            (
                Project: project
            );
        }
    }
}
