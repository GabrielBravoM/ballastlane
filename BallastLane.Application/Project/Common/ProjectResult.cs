﻿using BallastLane.Domain.Entities;

namespace BallastLane.Application.Project.Common
{
    public record ProjectResult(ProjectEntity Project);
}
