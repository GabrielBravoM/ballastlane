﻿namespace BallastLane.Contracts.Project
{
    public class CreateProjectRequest
    {
        public Guid UserExternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
    }
}
