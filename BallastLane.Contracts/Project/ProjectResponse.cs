﻿
namespace BallastLane.Contracts.Project
{
    public class ProjectResponse
    {
        public Guid ExternalId { get; set; }
        //public Guid UserExternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
    }
}
