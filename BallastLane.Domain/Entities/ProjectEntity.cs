﻿namespace BallastLane.Domain.Entities
{
    public class ProjectEntity
    {
        public int Id { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Owner { get; set; }
        public int UserId { get; set; }
    }
}
