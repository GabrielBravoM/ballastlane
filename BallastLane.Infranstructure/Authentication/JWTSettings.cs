﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BallastLane.Infranstructure.Authentication
{
    public class JWTSettings
    {
        public const string SectionName = "jwtSettings";
        public string Secret { get; init; } = null!;
        public string Issuer { get; init; } = null!;
        public int ExpirationMinutes { get; init; }
        public string Audience { get; init; } = null!;
    }
}
