﻿using BallastLane.Application.Common.Interfaces.Authentication;
using BallastLane.Domain.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BallastLane.Infranstructure.Authentication
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly JWTSettings _jwtSettings;

        public TokenGenerator(IOptions<JWTSettings> jwtSettings) { 
            _jwtSettings=  jwtSettings.Value;
        }

        public string GenerateToken(UserEntity user)
        {
            var credentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret)),
                SecurityAlgorithms.HmacSha256);
            
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.ExternalId.ToString()),
                new Claim(JwtRegisteredClaimNames.GivenName, user.FirstName),
                new Claim(JwtRegisteredClaimNames.FamilyName, user.LastName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var securityToken = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Audience,
                expires: DateTime.Now.AddDays(_jwtSettings.ExpirationMinutes),
                claims: claims,
                signingCredentials: credentials
                );
            
            return new JwtSecurityTokenHandler().WriteToken( securityToken );
        }
    }
}

