﻿using BallastLane.Application.Common.Interfaces.Authentication;
using BallastLane.Infranstructure.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Infranstructure.Persistance;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Options;

namespace BallastLane.Infranstructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfransctructure(
        this IServiceCollection services, 
        ConfigurationManager configuration)
    {
        services.AddAuth(configuration);
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IProjectRepository, ProjectRepository>();
        return services;
    }

    public static IServiceCollection AddAuth(
        this IServiceCollection services,
        ConfigurationManager configuration)
    {
        var jwtSettings = new JWTSettings();
        configuration.Bind(JWTSettings.SectionName, jwtSettings);

        services.AddSingleton(Options.Create(jwtSettings));
        services.AddSingleton<ITokenGenerator, TokenGenerator>();
        services.Configure<DBSettings>(configuration.GetSection(DBSettings.SectionName));

        services.AddAuthentication(defaultScheme: JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options => options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateIssuer= true,
                ValidateAudience = true,
                ValidateLifetime= true,
                ValidateIssuerSigningKey= true,
                ValidIssuer= jwtSettings.Issuer,
                ValidAudience= jwtSettings.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(jwtSettings.Secret))
            });


        return services;
    }
}
