﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BallastLane.Infranstructure.Persistance
{
    public class DBSettings
    {
        public const string SectionName = "Database";
        public string ConnectionString { get; init; } = null!;
    }
}
