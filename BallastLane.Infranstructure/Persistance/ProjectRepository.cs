﻿using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Domain.Entities;
using Dapper;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace BallastLane.Infranstructure.Persistance
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly static List<ProjectEntity> _projects = new();

        private readonly IOptions<DBSettings> _dbSettings;
        public ProjectRepository(IOptions<DBSettings> dbSettings)
        {
            _dbSettings = dbSettings;
        }

        public void CreateProject(ProjectEntity project)
        {
            using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
            var query = "Insert into Project(ExternalId, Name, Description, Owner, UserId) ";
            query += $" Values ('{project.ExternalId}','{project.Name}','{project.Description}','{project.Owner}',{project.UserId})";

            connection.Execute(query);
        }

        public void DeleteProject(string externalId)
        {
            using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
            var query = $"Delete From Project Where ExternalId='{externalId}'";

            connection.Execute(query);
        }

        public IEnumerable<ProjectEntity>? GetAll()
        {
            using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
            var projects = connection.Query<ProjectEntity>("Select Id, ExternalId, Name, Description, Owner, UserId From Project");

            return projects;
        }

        public ProjectEntity? GetByExternalId(string externalId)
        {
            using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
            var project = connection.Query<ProjectEntity>("Select Id, ExternalId, Name, Description, Owner, UserId From Project Where ExternalId='" + externalId + "' Limit 1");

            return project.FirstOrDefault();
        }

        public void UpdateProject(ProjectEntity project)
        {
            using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
            var query = $"Update Project Set Name='{project.Name}', Description='{project.Description}', Owner='{project.Owner}' Where ExternalId='{project.ExternalId}'";

            connection.Execute(query);
        }
    }
}
