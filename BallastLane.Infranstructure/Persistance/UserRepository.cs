﻿using BallastLane.Application.Common.Interfaces.Persistance;
using BallastLane.Domain.Entities;
using BallastLane.Infranstructure.Authentication;
using Microsoft.Extensions.Options;
using Dapper;
using MySql.Data.MySqlClient;

namespace BallastLane.Infranstructure.Persistance;

public class UserRepository : IUserRepository
{    
    private readonly IOptions<DBSettings> _dbSettings;
    public UserRepository(IOptions<DBSettings> dbSettings) {
        _dbSettings = dbSettings;
    }

    public void CreateUser(UserEntity user)
    {
        using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
        var query = "Insert into User(FirstName, LastName, Email, Password, ExternalId) ";
        query += $" Values ('{user.FirstName}','{user.LastName}','{user.Email}','{user.Password}','{user.ExternalId}')";

        connection.Execute(query);
    }

    public UserEntity? GetUserByEmail(string email)
    {
        using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
        var user = connection.Query<UserEntity>("Select Id, FirstName, LastName, Email, Password, ExternalId From User Where Email='" + email + "' Limit 1");

        return user.FirstOrDefault();
    }

    public UserEntity? GetUserByExternalId(string externalId) 
    {
        using var connection = new MySqlConnection(_dbSettings.Value.ConnectionString);
        var user = connection.Query<UserEntity>("Select Id, FirstName, LastName, Email, Password, ExternalId From User Where ExternalId='" + externalId + "' Limit 1");

        return user.FirstOrDefault();
    }
}
