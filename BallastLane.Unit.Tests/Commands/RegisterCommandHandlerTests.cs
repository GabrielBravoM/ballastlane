﻿using BallastLane.Application.Authentication.Commands.Register;
using BallastLane.Application.Common.Interfaces.Authentication;
using BallastLane.Application.Common.Interfaces.Persistance;
using NSubstitute;
using AutoFixture;
using BallastLane.Domain.Entities;
using FluentAssertions;
using BallastLane.Application.Common.Errors;

namespace BallastLane.Unit.Tests.Commands
{
    public class RegisterCommandHandlerTests
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly RegisterCommandHandler _serviceHandler;
        private readonly ITokenGenerator _tokenGenerator = Substitute.For<ITokenGenerator>();
        private readonly IUserRepository _userRepository = Substitute.For<IUserRepository>();

        public RegisterCommandHandlerTests() {
            _serviceHandler = new RegisterCommandHandler(_tokenGenerator, _userRepository);
        }

        [Fact]
        public async Task RegisterUser_GivenUserAlreadyExists_ThrowException()
        {
            var command = _fixture.Create<RegisterCommand>();
            var userEntity = _fixture.Create<UserEntity>();
            _userRepository.GetUserByEmail(command.Email).Returns(userEntity);

            var action = async () => await _serviceHandler.Handle(command, cancellationToken: new CancellationToken());

            await action.Should().ThrowAsync<DuplicatedEmailException>();
        }

        [Fact]
        public async Task RegisterUser_GivenNoExistingUser_ReturnsAuthenticationResult()
        {
            var command = _fixture.Create<RegisterCommand>();
            UserEntity? existingUserEntity = null;
            _userRepository.GetUserByEmail(command.Email).Returns(existingUserEntity);            
            
            var result = await _serviceHandler.Handle(command, cancellationToken: new CancellationToken());
            _userRepository.ReceivedWithAnyArgs().CreateUser(Arg.Any<UserEntity>());
            Assert.NotNull(result);
        }
    }
}
