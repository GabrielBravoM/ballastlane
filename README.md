# BallastLane
Test Application - Gabriel Bravo

[I run out of time for TDD implementation. I implemented a unit test for a command.]

## Requirement
This application includes the following funcionalities:
- User Admin
- Projects CRUD

## Use Cases
This applications manage the following use cases:
- Create User: Creates an User in database
- Login User: Authenticate an user and returns a JWT Token
- CRUD Projects: Every authenticated user can Create, Read, Update and Delete Products. This actions is only allowed for Authenticated users (private controllers)


## Components
The Application components are:
- Presentation: API and Contracts Projects
- Application: Use Cases 
- Domain: Entities
- Infranstructure: JWT and Database implementation


This solutions requires a MySQL database running locally (docker prefered)

### Local Setup
1. Start local MySQL
    ```
    docker run --name mysql -e MYSQL_ROOT_PASSWORD=ballastlane#2023 -p 3309:3306 -p 33062:33060 -d mysql:8.0.31
    ```

2. Connect to MySQL instance and run scripts stored in /Solutions Items/mysql

3. Execute the application (Presentation/BallastLane.Api should be set a start project)

4. Example Requests:
- Create User
 ```
    https://localhost:[PORT]/api/auth/register

    {
    "FirstName": "name",
    "LastName": "last",
    "Email": "mail@mail.com",
    "Password": "12345"
    }
```

- Login user
 ```
    https://localhost:[PORT]/api/auth/login

    
    {
    "Email": "mail@mail.com",
    "Password": "1234"
    }
```

- Create Project [Private]
 ```
    https://localhost:[PORT]/api/projects/create

    
   {
    "Name": "Project",
    "Description": "Description",
    "Owner": "Owner",
    "UserExternalId": "d76add4d-9689-4fa2-8931-a3ddd028a4d3"
    }
```

- Get Projects [Private]
```
    https://localhost:[PORT]/api/projects
```

- Update Project [Private]
```
    https://localhost:[PORT]/api/projects/2bc20f36-0474-4b6c-81da-5c7b53e96add

    {
    "ExternalId": "2bc20f36-0474-4b6c-81da-5c7b53e96add",
    "Name": "Project",
    "Description": "Description",
    "Owner": "Owner"
    }
```

- Delete Project [Private]
```
    https://localhost:[PORT]/api/projects/a9f0be00-e99f-40b9-8b45-05f60bab1f14
```

